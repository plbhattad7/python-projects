# This is my website, do visit:
# https://www.historycivics8.com/
# Do sign-up on my website.

# Importing Modules
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import getpass


# Account Details
username = input("Username/Email: ")
password = getpass.getpass()


# Making a Maximized Window
options = Options()
options.add_argument("start-maximized")
driver = webdriver.Chrome(options=options)


# Opens Twitter Login Page
driver.get("https://www.historycivics8.com/sign-in")


# Full XPATHS of the login Page
email_xpath = '//*[@id="user_login"]'
password_xpath = '//*[@id="user_pass"]'
login_xpath = '//*[@id="wppb-submit"]'

# Gives little time. Don't remove it you will have loads of errors
time.sleep(2)


# It logs you in your Twitter Account
driver.find_element_by_xpath(email_xpath).send_keys(username)
time.sleep(0.5)
driver.find_element_by_xpath(password_xpath).send_keys(password)
time.sleep(0.5)
driver.find_element_by_xpath(login_xpath).click()