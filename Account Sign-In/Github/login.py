# Importing Modules
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import getpass


# Account Details
username = input("Username/Email: ")
password = getpass.getpass()


# Making a Maximized Window
options = Options()
options.add_argument("start-maximized")
driver = webdriver.Chrome(options=options)


# Opens Github Login Page
driver.get("https://github.com/login")


# Full XPATHS of the login Page
email_xpath = '//*[@id="login_field"]'
password_xpath = '//*[@id="password"]'
login_xpath = '//*[@id="login"]/div[4]/form/input[14]'


# Gives little time. Don't remove it you will have loads of errors
time.sleep(2)


# It logs you in your Github Account
driver.find_element_by_xpath(email_xpath).send_keys(username)
time.sleep(0.5)
driver.find_element_by_xpath(password_xpath).send_keys(password)
time.sleep(0.5)
driver.find_element_by_xpath(login_xpath).click()