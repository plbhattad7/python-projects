# Importing Modules
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import getpass


# Account Details
username = input("Username/Email: ")
password = getpass.getpass()


# Making a Maximized Window
options = Options()
options.add_argument("start-maximized")
driver = webdriver.Chrome(options=options)


# Opens Facebook Login Page
driver.get("https://www.facebook.com/login")


# Full XPATHS of the login Page
email_xpath = '//*[@id="email"]'
password_xpath = '//*[@id="pass"]'
login_id = driver.find_element_by_id("loginbutton")


# Gives little time. Don't remove it you will have loads of errors
time.sleep(3)


# It logs you in your Facebook Account
driver.find_element_by_xpath(email_xpath).send_keys(username)
time.sleep(0.5)
driver.find_element_by_xpath(password_xpath).send_keys(password)
time.sleep(0.5)
login_id.click()