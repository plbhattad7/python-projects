# Importing Modules
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import getpass


# Account Details
email = input("Email: ")
password = getpass.getpass()


# Making a Maximized Window
options = Options()
options.add_argument("start-maximized")
driver = webdriver.Chrome(options=options)


# Opens Adobe Login Page
driver.get("https://accounts.adobe.com/")


# Waits for loading
time.sleep(8)


# Full XPATHS of the login Page
email_xpath = '//*[@id="EmailPage-EmailField"]'
email_continue = '//*[@id="EmailForm"]/section[2]/div[2]/button/span'
password_xpath = '//*[@id="PasswordPage-PasswordField"]'
login_xpath = '//*[@id="PasswordForm"]/section[2]/div[2]/button/span'


# Gives little time. Don't remove it you will have loads of errors
time.sleep(2)


# It logs you in your Adobe Account
driver.find_element_by_xpath(email_xpath).send_keys(email)
time.sleep(0.5)
driver.find_element_by_xpath(email_continue).click()
time.sleep(5)
driver.find_element_by_xpath(password_xpath).send_keys(password)
time.sleep(0.5)
driver.find_element_by_xpath(login_xpath).click()