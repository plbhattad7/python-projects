# Importing Modules
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import getpass


# Account Details
email = input("Email: ")
password = getpass.getpass()


# Making a Maximized Window
options = Options()
options.add_argument("start-maximized")
driver = webdriver.Chrome(options=options)


# Opens Twitter Login Page
driver.get("https://www.linkedin.com/login")


# Full XPATHS of the login Page
email_xpath = '//*[@id="username"]'
password_xpath = '//*[@id="password"]'
login_xpath = '//*[@id="organic-div"]/form/div[3]/button'


# Gives little time. Don't remove it you will have loads of errors
time.sleep(1)


# It logs you in your Twitter Account
driver.find_element_by_xpath(email_xpath).send_keys(email)
time.sleep(0.5)
driver.find_element_by_xpath(password_xpath).send_keys(password)
time.sleep(0.5)
driver.find_element_by_xpath(login_xpath).click()