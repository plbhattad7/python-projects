# Importing Modules
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import getpass


# Account Details
email = input("Email: ")
password = getpass.getpass()


# Making a Maximized Window
options = Options()
options.add_argument("start-maximized")
driver = webdriver.Chrome(options=options)


# Opens Bitbucket Login Page
driver.get("https://bitbucket.org/account/signin/")


# Full XPATHS of the login Page
email_xpath = '//*[@id="username"]'
email_continue = '//*[@id="login-submit"]/span/span/span'
password_xpath = '//*[@id="password"]'
login_xpath = '//*[@id="login-submit"]/span/span/span'


# Gives little time. Don't remove it you will have loads of errors
time.sleep(2)


# It logs you in your BitBucket Account
driver.find_element_by_xpath(email_xpath).send_keys(email)
time.sleep(0.5)
driver.find_element_by_xpath(email_continue).click()
time.sleep(3)
driver.find_element_by_xpath(password_xpath).send_keys(password)
time.sleep(0.5)
driver.find_element_by_xpath(login_xpath).click()