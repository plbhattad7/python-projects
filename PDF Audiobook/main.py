# Importing Modules
import pyttsx3
import PyPDF2
import tkinter as tk
from tkinter import *
from tkinter import filedialog

# Rooting Tkinter
root = tk.Tk()
root.withdraw()

# Selection of the PDF File
files =[('PDF Book', '*.pdf')]
pdf_path = filedialog.askopenfilename(initialdir = "/", title = "Choose the PDF Book", filetypes = files)

# Telling Python the book we are going to use
book = open(pdf_path, 'rb')
pdfReader = PyPDF2.PdfFileReader(book)
pages = pdfReader.numPages

# Initializing Speaker to Read the desired Book
speaker = pyttsx3.init()
for num in range(7, pages):
    page = pdfReader.getPage(num)
    text = page.extractText()
    speaker.say(text)
    speaker.runAndWait()