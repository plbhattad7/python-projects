# Text to Handwriting

This one of te greatest project to do your Homework.

Teacher have given you to write, you type and send her the image. But this will work for Students in 2020. 

So this is the Project in which you Convert Text to Handwriting in an Image.

You need to install this package: `pip install pyjokes`

This Project has an other version where every data will be stored in a `txt` file

The Image will be saved in your Parent Directory or the Folder.

You can change the Ink Color in the main file. you just have to change the RGB Value. By Default it is Black.

You can type, but if you want to have Line Breaks or Enter then you have to type: `\n`

And this only works for Latin Letters or English.
It won't have accents or Hindi or Chinese Letters.

Thankyou Please Star the **Repo** on Github:

https://github.com/plbhattad7/