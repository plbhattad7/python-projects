# Text to Handwriting

This one of te greatest project to do your Homework.

Teacher have given you to write, you insert the `txt` file and the the Image will be there. But this will work for Students in 2020. 

So this is the Project in which you Convert Text to Handwriting in an Image.

You need to install this package: `pip install pywhatkit`

This Project has an other version where you have to type and the work will be done.

The Image will be saved in your Parent Directory or the Folder.

You can change the Ink Color in the main file. you just have to change the RGB Value. By Default it is Black.

And this only works for Latin Letters or English.
It won't have accents or Hindi or Chinese Letters.

Thankyou Please Star the **Repo** on Github:

https://github.com/plbhattad7/