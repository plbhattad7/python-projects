# Importing Modules...
import tkinter as tk
import pywhatkit as kit
import os
from tkinter import filedialog
from tkinter import *

# Initializing Tkinter
root = tk.Tk()
root.withdraw()

# Selecting the Text File
file_extensions = [('Text Document', '*.txt')]
text_file = filedialog.askopenfilename(initialdir = "/", title = "Select a Text File", filetypes = file_extensions)

# Input or Type (English)
with open(text_file, 'r') as file:
    text = file.read()

# Conversion
kit.text_to_handwriting(text, rgb=[0,0,0])