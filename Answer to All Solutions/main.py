# Importing Modules
import wolframalpha

# Initializing Wolframalpha
client = wolframalpha.Client('9QLRHU-A28J3VK98X')

# Asking Questions and Finding Answers
query = input("What is the question?:")
res = client.query(query)
results = next(res.results).text

if query.isprintable():
    try:
        print(results)
        
    except StopIteration:
        print("No Results!")
        
    except AttributeError:
        print("There was a problem finding that!")
        
    except Exception:
        print("Check if you are connected to the internet")